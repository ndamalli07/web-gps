export const environment = {
  apiUrl: 'http://localhost:8081/',
  uriGpsPosition: 'api/gpspositions',
  uriCompareGpsPosition: '/isWithinDistance',
  SNACKBAR_DURATION_SUCCESS: 4000,
  SNACKBAR_DURATION_WARNING: 6000,
  SNACKBAR_DURATION_FAILED: 8000,
  SNACKBAR_CLASS_SUCCESS: 'custom-mat-snackbar-success',
  SNACKBAR_CLASS_WARNING: 'custom-mat-snackbar-warning',
  SNACKBAR_CLASS_ERROR: 'custom-mat-snackbar-error',
};
