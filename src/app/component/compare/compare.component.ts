import { GpsPosition } from 'src/app/Model/gpsposition';
import { GpsPositionService } from 'src/app/service/gps.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CustomSnackbarService } from 'src/app/service/custom-snackbar.service';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css'],
})
export class CompareComponent {
  selectedOption1!: number;
  selectedOption2!: number;
  options: any[] = [];
  inputdata: any;
  result: string = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: any,
    private ref: MatDialogRef<CompareComponent>,
    private formBuilder: FormBuilder,
    private readonly _service: GpsPositionService,
    private readonly _customSnackBarService: CustomSnackbarService
  ) {}
  ngOnInit(): void {
    this.inputdata = this.data;
    this.getData();
  }
  getData(): void {
    this._service.getGpsPositionList().subscribe((op: any[]) => {
      this.options = op;
    });
  }

  closepopup() {
    this.ref.close();
  }

  myform = this.formBuilder.group({
    position1: ['', [Validators.required]],
    position2: ['', [Validators.required]],
  });

  onSelectChange() {
    if (this.selectedOption1 && this.selectedOption2) {
        this.compareGpsPosition();
    }
    else {
      this.result = '';
    }
  }

  compareGpsPosition() {
    this._service
      .compareGpsPosition(this.selectedOption1, this.selectedOption2)
      .subscribe({
        next: (res) => {
          this.result = res
            ? 'La distance est inférieure à 10 KM'
            : 'La distance est supérieure à 10 KM';
        },
        error: (error: string) => {
          this._customSnackBarService.snackBarError('Erreur');
        },
      });
  }
}
