import { CompareComponent } from '../compare/compare.component';
import { CustomSnackbarService } from './../../service/custom-snackbar.service';
import { GpsPosition } from './../../Model/gpsposition';
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PopupComponent } from '../popup/popup.component';
import { GpsPositionService } from 'src/app/service/gps.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent {
  gpsPositionslist!: GpsPosition[];
  dataSource: any;
  displayedColumns: string[] = ['position', 'latitude', 'longitude', 'action'];
  @ViewChild('paginator') paginatior!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private readonly _service: GpsPositionService,
    private readonly _customSnackBarService: CustomSnackbarService,
    private dialog: MatDialog
  ) {
    this.loadGpsPosition();
  }

  loadGpsPosition() {
    this._service.getGpsPositionList().subscribe({
      next: (res) => {
        this.gpsPositionslist = res;
        this.dataSource = new MatTableDataSource<GpsPosition>(
          this.gpsPositionslist
        );
        this.dataSource.paginator = this.paginatior;
        this.dataSource.sort = this.sort;
      },
      error: (error: string) => {
        this._customSnackBarService.snackBarError(
          'Erreur chargement des positions'
        );
      },
    });
  }

  deleteGpsPosition(id: number) {
    const confirmed = window.confirm(
      'Voulez-vous vraiment supprimer cette position ?'
    );
    if (confirmed) {
      this._service.deleteGpsPosition(id).subscribe({
        next: (res) => {
          this._customSnackBarService.snackBarSuccess('Opération réussie');
          this.loadGpsPosition();
        },
        error: (error: string) => {
          this._customSnackBarService.snackBarError(
            'Erreur suppression position'
          );
        },
      });
    }
  }

  addGpsPosition() {
    this.openpopup('Nouvelle Position GPS', PopupComponent);
  }

  compareGpsPosition() {
    this.openpopup('Comparer 2 GPS (+)', CompareComponent);
  }

  openpopup(title: any, component: any) {
    var _popup = this.dialog.open(component, {
      width: '40%',
      enterAnimationDuration: '100ms',
      exitAnimationDuration: '100ms',
      data: {
        title: title,
      },
    });
    _popup.afterClosed().subscribe((item) => {
      this.loadGpsPosition();
    });
  }
}
