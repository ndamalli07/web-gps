import { GpsPosition } from 'src/app/Model/gpsposition';
import { GpsPositionService } from 'src/app/service/gps.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CustomSnackbarService } from 'src/app/service/custom-snackbar.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
})
export class PopupComponent implements OnInit {
  inputdata: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: any,
    private ref: MatDialogRef<PopupComponent>,
    private formBuilder: FormBuilder,
    private readonly _service: GpsPositionService,
    private readonly _customSnackBarService: CustomSnackbarService
  ) {}

  ngOnInit(): void {
    this.inputdata = this.data;
  }

  closepopup() {
    this.ref.close();
  }

  myform = this.formBuilder.group({
    position: ['', [Validators.required]],
    latitude: [
      '',
      [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,4})?$')],
    ],
    longitude: [
      '',
      [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,4})?$')],
    ],
  });

  addGpsPosition() {
    console.log(this.myform.controls);
    const gpsPosition = {
      id: 0,
      position: this.myform.controls.position.value,
      latitude: this.myform.controls.latitude.value,
      longitude: this.myform.controls.longitude.value,
    } ;
    this._service.addGpsPosition(gpsPosition).subscribe({
      next: (res) => {
        this._customSnackBarService.snackBarSuccess('Opération réussie');
        this.closepopup();
      },
      error: (error: string) => {
        this._customSnackBarService.snackBarError(
          'Erreur suppression position'
        );
      },
    });
  }
}
