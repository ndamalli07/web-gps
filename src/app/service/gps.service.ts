import { environment } from './../../environments/environment';
import { GpsPosition } from 'src/app/Model/gpsposition';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GpsPositionService {

  private readonly uri = environment.apiUrl.concat(environment.uriGpsPosition);
  private readonly uriCompare = this.uri.concat(environment.uriCompareGpsPosition);

  constructor(private readonly http: HttpClient) { }

  getGpsPositionList():Observable<GpsPosition[]>{
    return this.http.get<GpsPosition[]>(this.uri);
  }

  deleteGpsPosition(id: number) {
    return this.http.delete(`${this.uri}/${id}`);
  }

  addGpsPosition(data:any){
    return this.http.post(this.uri,data);
  }

  compareGpsPosition(position1Id: number,position2Id: number){
    return this.http.get(`${this.uriCompare}/${position1Id}/${position2Id}`);
  }

}
