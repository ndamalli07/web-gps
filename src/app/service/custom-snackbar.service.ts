import { environment } from './../../environments/environment';
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: "root",
})
export class CustomSnackbarService {

  constructor(private readonly _matSnackBar: MatSnackBar) {}

  snackBarSuccess(message: string, duration?: number): void {
    this._matSnackBar.open(message, "OK", {
      verticalPosition: "top",
      duration: duration ?? environment.SNACKBAR_DURATION_SUCCESS,
      panelClass: environment.SNACKBAR_CLASS_SUCCESS,
    });
  }

  snackBarWarning(message: string, duration?: number): void {
    this._matSnackBar.open(message, "Close", {
      verticalPosition: "top",
      duration: duration ?? environment.SNACKBAR_DURATION_WARNING,
      panelClass: environment.SNACKBAR_CLASS_WARNING,
    });
  }

  snackBarError(message: string, duration?: number): void {
    this._matSnackBar.open(message, "Close", {
      verticalPosition: "top",
      duration: duration ?? environment.SNACKBAR_DURATION_FAILED,
      panelClass: environment.SNACKBAR_CLASS_ERROR,
    });
  }
}