export interface GpsPosition {
  id: number;
  position: string;
  latitude: number;
  longitude: number;
}